#' @title helper: query person details
#'
#' @description Helper function to query person details (name, initials).
#'
#' @export
#'
query_persons <- function() {

  base_query <- "
  SELECT
    id,
    last_name,
    first_name,
    initials
  FROM herpetofauna.persons
  ORDER BY initials
  ;
  "

  persons <- DBI::dbGetQuery(
    conn      = this_pool,
    statement = base_query
  )

  return(persons)

}

persons <- query_persons()

#' @title helper: query herpetofauna sampling event data
#'
#' @description The function included here ( query_sampling_events_all )
#' queries herpetofauna sampling event data (all) from the database.
#'
#' @export
#'
query_sampling_events_all <- function() {

  base_query <- "
  SELECT
    sampling_events.id,
    sampling_events.river_reaches_id,
    river_reaches.reach,
    surveyors.initials,
    sampling_events.observation_date::DATE,
    sampling_events.sampling_events_notes
  FROM
    herpetofauna.sampling_events
  JOIN herpetofauna.river_reaches ON (river_reaches.id = sampling_events.river_reaches_id)
  LEFT JOIN (
    SELECT
      sampling_events.id,
      array_to_string(array_agg(persons.initials), '; ') AS initials
    FROM
      herpetofauna.sampling_events
    JOIN herpetofauna.observers ON (observers.sampling_event_id = sampling_events.id)
    JOIN herpetofauna.persons ON (persons.id = observers.person_id)
    GROUP BY
      sampling_events.id
  ) AS surveyors ON (surveyors.id = sampling_events.id)
  ORDER BY 
    sampling_events.id DESC
  ;
  "
  parameterized_query <- DBI::sqlInterpolate(
    DBI::ANSI(),
    base_query
  )

  run_interpolated_query(parameterized_query)

}

#' @title helper: insert survey observation
#'
#' @description Helper query to insert a survey observation.
#'
#' @export
#'
insert_survey_observation <- function(
  survey_id,
  taxon_code,
  quantity,
  note
  ) {

  note <- gsub("[\r\n]", "; ", note)
  note <- gsub(",", ";", note)

  parameterized_query <- glue::glue_sql("
    INSERT INTO herpetofauna.surveys_observations (
      survey_id,
      herp_taxon_id,
      quantity,
      surveys_observation_notes
    )
    VALUES (
      {as.integer(survey_id)},
      {taxa[taxa$code == taxon_code,]$id},
      {as.integer(quantity)},
      {note}
    )
    ;
    ",
    .con = DBI::ANSI()
  )

  run_interpolated_execution(
    interpolatedQuery = parameterized_query,
    show_notification = FALSE
  )

  # message("from insert surey obs\n", parameterized_query)

}

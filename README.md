## herpetofauna-shiny

### R Shiny application to facilitate CAP LTER herpetofauna data entry and quality control

This is the codebase for the Shiny application that facilitates data entry and quality control for the CAP LTER herpetofauna long-term monitoring program.

The application introduces several new approaches to Shiny development:

1. A, possibly, more efficient approach to adding interactivity to tables that draws on button configurations in a .js file. From:
  + [blog](https://www.tychobra.com/posts/2020-01-29-shiny-crud-traditional/)
  + [source code](https://github.com/Tychobra/shiny_crud)
2. Establishing a 'listener' in session\$userData to facilitate interactivity across modules, notably across tabs in this application. Whereas earlier applications forced the user to reload the app so that changes made on one tab were accessible on another tab, here we use session, which is global, to enable updates across modules without reloading the app. There is some debate about whether this is a good approach, in fact Hadley specifically warns against this in [Mastering Shiny](https://mastering-shiny.org/scaling-modules.html). On the flip side, some very reputable practitioners (e.g., [Colin Fay](https://github.com/ColinFay/gargoyle), [Appsilon](https://appsilon.com/super-solutions-for-shiny-architecture-1-of-5-using-session-data/), and [Barbara Borges Ribeiro and Dean Atalli](https://github.com/rstudio/shiny/issues/1546)) do capitalize on session. I think generally as detailed in the comments section of the Appsilon post is that capitalizing on session is acceptable but there are tradeoffs and to be explicit about when, where, and how the application is using session.
3. Using modals (see the links in point one for reference here also). The modal dialogue box is a really nice feature that, particularly, can make editing existing records for more efficient.
4. Not particularly new but different here is that we are employing search and filter functionality provided by DataTables. This is really convenient and works ~~fine~~ great for the relatively small number of records in this database but may have to be revised as the database grows (but that would be long down-the-road for this project).
